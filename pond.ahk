ToolTip, Click the four corners
Sleep, 2000

ToolTip, Top Left
KeyWait, LButton, D
MouseGetPos, tl_xpos, tl_ypos
Sleep 500

ToolTip, Bottom Right
KeyWait, LButton, D
MouseGetPos, br_xpos, br_ypos
Sleep 500

SetTimer, Hop, 1

Esc::BreakLoop = 1

Hop:
ToolTip
Loop {
    if (BreakLoop = 1)
        break
    
    PixelSearch, px, py, tl_xpos, tl_ypos, br_xpos, br_ypos, 0x789924, 5, Fast RGB
    if ErrorLevel
        ; ToolTip, Couldn't find a matching pixel
        BreakLoop = 1
    else
        MouseMove, px, py
        MouseClick, left
    Sleep 1200
}

ExitApp