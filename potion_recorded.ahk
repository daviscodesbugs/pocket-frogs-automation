Sleep, 3000
MsgBox, Click on the center of the potion box
ToolTip, Click on center of potion box, 10, 10
KeyWait, LButton, D
MouseGetPos, potion_xpos, potion_ypos, potion_window_id

MsgBox, Wait for 30 seconds, then close the ad
ToolTip, Close ad when done, 10, 10
KeyWait, LButton, D
MouseGetPos, close_xpos, close_ypos, close_window_id

MsgBox, Hit okay, then walk away!
ToolTip, Click continue, 10, 10
KeyWait, LButton, D
MouseGetPos, okay_xpos, okay_ypos, okay_window_id
ToolTip, Starting automation..., 10, 10
Sleep, 3000

Loop {
	; Start ad
	ToolTip, Starting ad, 10, 10
	Random, xrand, -30, 30
	Random, yrand, -30, 30
	Random, trand, 0, 10
	MouseMove, potion_xpos + xrand, potion_ypos + yrand, 10 + trand
	MouseClick, left
	WaitForAd()

	; Close ad
	ToolTip, Closing ad, 10, 10
	Random, xrand, -3, 3
	Random, yrand, -3, 3
	Random, trand, 0, 10
	MouseMove, close_xpos + xrand, close_ypos + yrand, 10 + trand
	MouseClick, left
	ToolTip, Waiting confirmation, 10, 10
 	Sleep, 1000

	; Close confirmation
	ToolTip, Closing confirmation, 10, 10
	Random, xrand, -10, 10
	Random, yrand, -5, 5
	Random, trand, 0, 10
	MouseMove, okay_xpos + xrand, okay_ypos + yrand, 10 + trand
	MouseClick, left
	ToolTip, Waiting for new cycle, 10, 10
	Sleep, 1000
}

WaitForAd() {
	Loop, 32 {
		diff := 32 - A_Index
		ToolTip, Waiting for ad %diff%..., 10, 10
		Sleep, 1000
	}
}